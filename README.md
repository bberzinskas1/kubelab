# billieblaze's home kubelab

I've been getting into kubernetes more and more at work and I wanted a small scale home environment to do experiments, run some projects and more importantly, LEARN!   

This repo will hold all the project information, from the 3d printed models used to the ansible / terraform / whatever else I end up using.   

## Building the "Rack"

I stumbled upon the "SixInchRack" (https://github.com/KronBjorn/SixInchRack) on Thingiverse.com and was hooked.  A totally modular mini-rack for nerds building compute clouds, how convenient!  

I had several sticks of aluminum extrusion on hand already and I grabbed 4 bits that were about the same size. I did the math and the nearest size turned out to be "31U" of rack height.  I used the OpenSCAD models in the project to design and export the proper sized rails. I also printed the vented top and open bottom, as well as a set of handles for the top. I found some rubber feet in my parts stash and with only 8xM4 screws, assembly was a breeze!

All said, I've got about 2 days worth of printing with a medium density setting.   (I'll be trying my bigger nozzle soon!)

## Installing Power

There are two power rails needed, a 5v for the switch and any PI-like devices and a 12v for the Intel NUC's I'll be using.

For 5v, I used a Meanwell RS-50-5 (https://www.meanwell-web.com/content/files/pdfs/productPdfs/MW/RS-50/RS-50-spec.pdf) which outputs 5v @ 10A. I used this for the "rack enclosure":
https://www.thingiverse.com/thing:2464709

For 12v, I used a Meanwell RPS-400-12 (https://www.meanwell-web.com/content/files/pdfs/productPdfs/MW/RPS-400/RPS-400-spec.pdf) which outputs 12v @ 33A.  I made a slight modification (meanwell_psu.stl/.skp) to the meanwell rack above with different holes.  

Each power rail is fed to a switchable power distribution unit (https://www.thingiverse.com/thing:2896748).  To start, this is simply a spade end cable, a _bus bar_, several switches and an equal number of DC barrel jacks.  
The wiring is straightforward, all grounds get tied together, resistor from each led to the switch, same switch node to all the barrel jacks and 5v to the other side of the switch.

While wiring up the AC, I wanted to reuse some parts I had lying around rather than spend more amazonbucks(tm).  I forked `designs/enclosure.scad` and added part "L", a 4u back plate with an IEC plug, 2 fuse holders and two rocker switches.  

# Installing the NUCs

Using the SixInchRack .scad files, I added part `g:nuc enclosure`.  It should fit a nuc in a 3u space, it does not have anything fancy on the front cover, the lid is a few mm to short and i'm not using a back plate currently.  

Once installed and wired to the power distro's, I setup each one's bios.  The most important thing is to set it to turn back on if it loses power, especially because the button is hidden in the case.   

# Setting up the kubernetes nodes

All of the NUC's have SATA, but i currently only have one SSD, so the master node gets it (for now at least).  Other nodes will operate off a 64gb usb3.1 flash drive.  I did try to install on eMMC but didn't have any luck.  I located each node on the network, assigned it a static IP and used `ssh-copy-id` to get an ssh key on each one.  Once they were all online, I added them to `hosts` and ran the `ansible/provision.yml` playbook.

I flashed a spare flash drive with the ubuntu installer and installed in a similar way on each one.  I gave it a single user, a static IP (192.x.x.100-103) and added my SSH key to it.   Once the keys were in place, I started building out `ansible/setup_k8s.yml`. This script is responsible for creating the cluster on the master and joining on the nodes.

Once the cluster is up, `ansible/install_services.yml` installs things into the functioning cluster.

OpenEBS provides storage. I had to label the nodes, at some point i want to run this on the master only w/ its big ssd.   `kubectl label nodes <nodename> node=openebs`

Metallb is configured to pull ips in the 192.168.x.200-250 range for any kubernetes `LoadBalancer` object.  I next setup Traefik as ingress controller.

Traefik pulls the first IP in the range, and I open its web ports (80/443) on my router.  This should be the only public entry point.  

Prometheus will collect metrics. Grafana will visualize them.  

At this point, I've got a functioning / observable cluster.  HUZZAH!!    

# Adding ARM64
I've got a few PI3 , might as well hook them up too!    I flashed their SD cards with Ubuntu 20.04 as well.   Created my ansible user and gave them each a static ip 192.x.x.15x = pi's  (https://linuxize.com/post/how-to-configure-static-ip-address-on-ubuntu-20-04/).    Once online, the same playbooks (provison + setup_k8s) bring them into the cluster.    

# Tearing it down

Inevitably shit breaks, or I want to practice some of the automation.  `ansible/remove_k8s.yml` will force delete everything allowing the setup playbook to be run again.


# Managing system-level service installation... WITH ARGOCD!  

Initially, I did all of the initial install of things like metallb and traefik with tedious ansible scripts.   I'd been checking out ArgoCD for quite some time and decided to really dig into it.   Argo manages the state of packages in the cluster.  It's super nice and can technically even install and manage itself (more on this later).  For now though, when the cluster is provisioned, it gets `flannel` for in-cluster networking and `argocd`, nothing else.   As nodes come online and argo stands up, it begins to auto-provion the an app-of-apps that I've dubbed `system-apps`.  This package contains metallb, prometheus, etc etc which argo happily configures + deploys.

# HA Control Plane
https://github.com/kubernetes/kubeadm/blob/master/docs/ha-considerations.md

I'm currently setting up HAProxy on one of the RasPi's (probably both RasPi if we're being honest).  This will load balance multiple kubernetes control plane nodes with a few simple flags.   
